package com.groundgurus.sbmultipledatasourceexample.dao.db2;

import com.groundgurus.sbmultipledatasourceexample.entity.db2.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectDao extends JpaRepository<Subject, Long> {
  Subject findFirstByCode(String code);
}

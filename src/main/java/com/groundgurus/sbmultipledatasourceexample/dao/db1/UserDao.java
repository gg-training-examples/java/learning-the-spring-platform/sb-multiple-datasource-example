package com.groundgurus.sbmultipledatasourceexample.dao.db1;

import com.groundgurus.sbmultipledatasourceexample.entity.db1.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDao extends JpaRepository<User, Long> {
  Optional<User> findUserByUsername(String username);
}

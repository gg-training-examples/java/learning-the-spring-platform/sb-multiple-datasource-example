package com.groundgurus.sbmultipledatasourceexample.dao.db1;

import com.groundgurus.sbmultipledatasourceexample.entity.db1.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface StudentDao extends JpaRepository<Student, Long> {
  @Query("SELECT s FROM Student s WHERE s.firstName = :firstName AND s.lastName = :lastName")
  Optional<Student> findByName(String firstName, String lastName);

  Optional<Student> findFirstByStudentId(String studentId);

}

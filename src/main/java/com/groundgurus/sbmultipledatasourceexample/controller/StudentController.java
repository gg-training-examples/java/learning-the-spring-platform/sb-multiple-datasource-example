package com.groundgurus.sbmultipledatasourceexample.controller;

import com.groundgurus.sbmultipledatasourceexample.service.SchoolService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {
  private SchoolService schoolService;

  public StudentController(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @GetMapping("/")
  String index() {

    return "index";
  }

  @GetMapping("/login")
  String login() {
    return "login";
  }

  @GetMapping("/students")
  String students(Model model) {
    model.addAttribute("students", schoolService.getStudents());
    return "students/students";
  }
}

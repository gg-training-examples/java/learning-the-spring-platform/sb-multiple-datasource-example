package com.groundgurus.sbmultipledatasourceexample.entity.db1;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
public class Student {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String studentId;
  private String firstName;
  private String lastName;
  private String subjects;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @Transient
  private User user;

  public Student() {
  }

  public Student(String studentId, String firstName, String lastName, String subjects, User user) {
    this.studentId = studentId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.subjects = subjects;
    this.user = user;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getStudentId() {
    return studentId;
  }

  public void setStudentId(String studentId) {
    this.studentId = studentId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getSubjects() {
    return subjects;
  }

  public void setSubjects(String subjects) {
    this.subjects = subjects;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}

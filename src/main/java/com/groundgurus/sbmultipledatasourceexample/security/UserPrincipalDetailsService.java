package com.groundgurus.sbmultipledatasourceexample.security;

import com.groundgurus.sbmultipledatasourceexample.dao.db1.UserDao;
import com.groundgurus.sbmultipledatasourceexample.entity.db1.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
  private UserDao userDao;

  public UserPrincipalDetailsService(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<User> searchUsername = userDao.findUserByUsername(username);
    return searchUsername.orElseThrow();
  }
}

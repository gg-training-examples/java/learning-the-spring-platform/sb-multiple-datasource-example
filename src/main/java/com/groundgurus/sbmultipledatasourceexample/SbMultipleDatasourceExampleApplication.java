package com.groundgurus.sbmultipledatasourceexample;

import com.groundgurus.sbmultipledatasourceexample.dao.db1.StudentDao;
import com.groundgurus.sbmultipledatasourceexample.dao.db1.UserDao;
import com.groundgurus.sbmultipledatasourceexample.dao.db2.SubjectDao;
import com.groundgurus.sbmultipledatasourceexample.entity.db1.Student;
import com.groundgurus.sbmultipledatasourceexample.entity.db1.User;
import com.groundgurus.sbmultipledatasourceexample.entity.db2.Subject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootApplication
public class SbMultipleDatasourceExampleApplication {

  public static void main(String[] args) {
    SpringApplication.run(SbMultipleDatasourceExampleApplication.class, args);
  }

  @Bean
  @Transactional("studentTransactionManager")
  public CommandLineRunner runner(UserDao userDao, StudentDao studentDao, SubjectDao subjectDao,
                                  PasswordEncoder passwordEncoder) {
    return args -> {
      // clean up
      userDao.deleteAll();

      // create the user accounts
      List<User> users = List.of(
        new User("john_doe", passwordEncoder.encode("password"), "STUDENT"),
        new User("mary_poppins", passwordEncoder.encode("password"), "STUDENT,COUNCIL")
      );
      userDao.saveAll(users);

      // clean up
      studentDao.deleteAll();

      // create the list of students
      List<Student> students = List.of(
        new Student("sc201901", "John", "Doe", "cs01,cs02", userDao.findUserByUsername("john_doe").get()),
        new Student("sc201902", "Mary", "Poppins", "cs01,cs02,cs03", userDao.findUserByUsername("mary_poppins").get())
      );
      studentDao.saveAll(students);

      // clean up
      subjectDao.deleteAll();

      // create the list of subjects
      List<Subject> subjects = List.of(
        new Subject("Introduction to Computing", "cs01"),
        new Subject("Getting Started with C", "cs02"),
        new Subject("The Java Programming Language", "cs03")
      );
      subjectDao.saveAll(subjects);
    };
  }
}

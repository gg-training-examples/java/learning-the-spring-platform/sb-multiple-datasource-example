package com.groundgurus.sbmultipledatasourceexample.dto;

import java.util.List;

public class StudentDto {
  private String studentId;
  private String firstName;
  private String lastName;
  private List<SubjectDto> subjects;

  public StudentDto() {
  }

  public StudentDto(String studentId, String firstName, String lastName) {
    this.studentId = studentId;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public StudentDto(String studentId, String firstName, String lastName, List<SubjectDto> subjects) {
    this.studentId = studentId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.subjects = subjects;
  }

  public String getStudentId() {
    return studentId;
  }

  public void setStudentId(String studentId) {
    this.studentId = studentId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public List<SubjectDto> getSubjects() {
    return subjects;
  }

  public void setSubjects(List<SubjectDto> subjects) {
    this.subjects = subjects;
  }
}

package com.groundgurus.sbmultipledatasourceexample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:persistence-multiple-db.properties"})
@EnableJpaRepositories(
  basePackages = "com.groundgurus.sbmultipledatasourceexample.dao.db1",
  entityManagerFactoryRef = "studentEntityManager",
  transactionManagerRef = "studentTransactionManager"
)
public class StudentConfig {
  private Environment env;

  public StudentConfig(Environment env) {
    this.env = env;
  }

  @Bean
  @Primary
  public LocalContainerEntityManagerFactoryBean studentEntityManager() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(studentDataSource());
    em.setPackagesToScan("com.groundgurus.sbmultipledatasourceexample.entity.db1");

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
    properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
    em.setJpaPropertyMap(properties);

    return em;
  }

  @Bean
  public DataSource studentDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
    dataSource.setUrl(env.getProperty("student.jdbc.url"));
    dataSource.setUsername(env.getProperty("student.jdbc.user"));
    dataSource.setPassword(env.getProperty("student.jdbc.pass"));
    return dataSource;
  }

  @Bean
  public PlatformTransactionManager studentTransactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(studentEntityManager().getObject());
    return transactionManager;
  }
}

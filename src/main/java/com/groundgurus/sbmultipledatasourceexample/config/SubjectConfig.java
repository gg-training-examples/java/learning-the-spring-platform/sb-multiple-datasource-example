package com.groundgurus.sbmultipledatasourceexample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:persistence-multiple-db.properties"})
@EnableJpaRepositories(
  basePackages = "com.groundgurus.sbmultipledatasourceexample.dao.db2",
  entityManagerFactoryRef = "subjectEntityManager",
  transactionManagerRef = "subjectTransactionManager"
)
public class SubjectConfig {
  private Environment env;

  public SubjectConfig(Environment env) {
    this.env = env;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean subjectEntityManager() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(subjectDataSource());
    em.setPackagesToScan("com.groundgurus.sbmultipledatasourceexample.entity.db2");

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
    properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
    em.setJpaPropertyMap(properties);

    return em;
  }

  @Bean
  public DataSource subjectDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
    dataSource.setUrl(env.getProperty("subject.jdbc.url"));
    dataSource.setUsername(env.getProperty("subject.jdbc.user"));
    dataSource.setPassword(env.getProperty("subject.jdbc.pass"));
    return dataSource;
  }

  @Bean
  public PlatformTransactionManager subjectTransactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(subjectEntityManager().getObject());
    return transactionManager;
  }
}

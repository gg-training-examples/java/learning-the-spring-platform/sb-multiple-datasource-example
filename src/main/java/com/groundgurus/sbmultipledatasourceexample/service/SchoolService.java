package com.groundgurus.sbmultipledatasourceexample.service;

import com.groundgurus.sbmultipledatasourceexample.dao.db1.StudentDao;
import com.groundgurus.sbmultipledatasourceexample.dao.db2.SubjectDao;
import com.groundgurus.sbmultipledatasourceexample.dto.StudentDto;
import com.groundgurus.sbmultipledatasourceexample.dto.SubjectDto;
import com.groundgurus.sbmultipledatasourceexample.entity.db1.Student;
import com.groundgurus.sbmultipledatasourceexample.exception.SchoolException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SchoolService {
  private StudentDao studentDao;
  private SubjectDao subjectDao;

  public SchoolService(StudentDao studentDao, SubjectDao subjectDao) {
    this.studentDao = studentDao;
    this.subjectDao = subjectDao;
  }

  public List<StudentDto> getStudents() {
    return studentDao.findAll()
      .stream()
      .map(student -> new StudentDto(
        student.getStudentId(),
        student.getFirstName(),
        student.getLastName(),
        getStudentSubjects(student)))
      .collect(Collectors.toList());
  }

  @Transactional
  public void updateStudent(String studentId, Student student) {
    Optional<Student> searchedStudent = studentDao.findFirstByStudentId(studentId);
    if (searchedStudent.isPresent()) {
      Student retrievedStudent = searchedStudent.get();
      retrievedStudent.setFirstName(student.getFirstName());
      retrievedStudent.setLastName(student.getLastName());
      studentDao.save(retrievedStudent);
    } else {
      throw new SchoolException(String.format("Student with id %s is not found", studentId));
    }
  }

  @Transactional
  public void deleteStudent(String studentId) {
    Optional<Student> searchedStudent = studentDao.findFirstByStudentId(studentId);
    if (searchedStudent.isPresent()) {
      Student retrievedStudent = searchedStudent.get();
      studentDao.delete(retrievedStudent);
    }
  }

  private List<SubjectDto> getStudentSubjects(Student student) {
    return List.of(student.getSubjects().split(","))
      .stream()
      .map(subjectStr -> subjectDao.findFirstByCode(subjectStr))
      .map(subject -> new SubjectDto(subject.getName(), subject.getCode()))
      .collect(Collectors.toList());
  }
}

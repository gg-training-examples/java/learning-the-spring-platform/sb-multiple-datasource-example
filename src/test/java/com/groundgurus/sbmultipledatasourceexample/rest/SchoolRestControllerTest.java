package com.groundgurus.sbmultipledatasourceexample.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SchoolRestControllerTest {
  @Mock
  private SchoolRestController schoolRestController;

  @Test
  public void testHelloWorld() {
    String name = "World";
    when(schoolRestController.message(name)).thenReturn("Hello World");

    assertEquals("Hello World", schoolRestController.message(name));
  }

  @Test(expected = RuntimeException.class)
  public void testHelloWorld_NoNamePassed() {
    when(schoolRestController.message(null)).thenThrow(new RuntimeException("No name passed"));
    schoolRestController.message(null);
  }
}

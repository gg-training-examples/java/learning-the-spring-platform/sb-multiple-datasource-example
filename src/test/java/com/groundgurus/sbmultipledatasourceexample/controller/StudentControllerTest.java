package com.groundgurus.sbmultipledatasourceexample.controller;

import com.groundgurus.sbmultipledatasourceexample.dto.StudentDto;
import com.groundgurus.sbmultipledatasourceexample.service.SchoolService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {
  @Mock
  private StudentController studentController;
  @Mock
  private SchoolService schoolService;
  private List<StudentDto> students;

  @Before
  public void setUp() throws Exception {
    students = List.of(new StudentDto("123", "John", "Doe"));
  }

  @Test
  public void students() {
    ExtendedModelMap model = new ExtendedModelMap();
    when(studentController.students(model)).thenReturn("students/students");

    String nextPage = studentController.students(model);

    assertEquals("students/students", nextPage);
  }

}

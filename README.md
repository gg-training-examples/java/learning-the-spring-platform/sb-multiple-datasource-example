# Spring Boot Multiple Databases
This is an example of handling multiple databases


## References
- Multiple database example
https://www.baeldung.com/spring-data-jpa-multiple-databases

- Spring Security that includes session for web and jwt for the REST API
https://stackoverflow.com/questions/44970848/spring-security-jwt-token-for-api-and-session-for-web
- Exception Handling
https://www.baeldung.com/exception-handling-for-rest-with-spring
